from sklearn.neural_network import MLPClassifier
import numpy as np
import pandas as pd
# Doc du lieu
df = pd.read_excel('data.xlsx')
X = df.iloc[:, :-1].values
y = df.iloc[:, -1].values

# Tach du lieu
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)

# Tien xu ly du lieu: standardization (chuan hoa)
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
sc = Normalizer() #StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Dinh nghia thuat toan hoc may/ huan luyen
predictor = MLPClassifier(random_state=6, max_iter=5000, hidden_layer_sizes=(3,), activation='relu')
predictor.fit(X_train, y_train)

# Du bao, danh gia
print(predictor.predict(X_test))
print(predictor.score(X_test, y_test))
