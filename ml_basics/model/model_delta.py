# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import pickle
# TH1: Doc du lieu voi pandas - csv, excel, json
dataset = pd.read_excel('data.xlsx')
# xoa cac dong trung lap
dataset.drop_duplicates(inplace=True)

# xoa cac gia tri bat thuong (lon, nho)
for x in dataset.index:
    if dataset.loc[x, "T"] > 50:
        dataset.loc[x, "T"] = dataset['T'].mean()

X = dataset.iloc[:, :-1].values                #doc toan bo Hang, cot doc tu 0 -> truoc cot cuoi
y = dataset.iloc[:, -1].values                  #doc toan bo Hang, cot cuoi cung
y *= 1e5
print(X[0:10])
print(y[0:10])

# xu l du lieu bi thieu.
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
imputer.fit(X)                          # xu ly nhung cot kieu so  
X = imputer.transform(X)

# chia tap du lieu ban dau thanh 2 tap: Train - Test
from sklearn.model_selection import train_test_split
# Luu y: test_size = 0.2 co nghia la lay 20% de Test - 80% Train
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state= 1)

# Khai bao mo hinh mong muon (RandomForestRegressor)
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor
# Cai dat mo hinh voi cac tham so thuat toan
# regressor = MLPRegressor(random_state = 1, hidden_layer_sizes=(5,),activation="logistic", solver="lbfgs", max_iter=5000)
regressor = RandomForestRegressor(n_estimators=150, criterion="absolute_error")
# huan luyen dua tren tap du lieu (Train)
regressor.fit(X_train, y_train)

# Danh gia
# voi bai toan Hoi quy (regression): R2 score
print(regressor.score(X_test, y_test))

filename = 'model_reg.sav'
pickle.dump(regressor, open(filename, 'wb'))
