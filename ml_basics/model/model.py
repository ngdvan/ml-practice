# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
# TH1: Doc du lieu voi pandas - csv, excel, json
dataset = pd.read_csv('Position_Salaries.csv')
# xoa cac dong trung lap
dataset.drop_duplicates(inplace=True)

# xoa cac gia tri bat thuong (lon, nho)
for x in dataset.index:
    if dataset.loc[x, "year"] > 50:
        dataset.loc[x, "year"] = dataset['year'].mean()

X = dataset.iloc[:, :-1].values                #doc toan bo Hang, cot doc tu 0 -> truoc cot cuoi
y = dataset.iloc[:, -1].values                  #doc toan bo Hang, cot cuoi cung



# TH2: Doc du lieu tu file xuat ra tu Numpy
# dataset = np.loadtxt('data.reg', delimiter=' ')
# X = dataset[:,:-1]
# y = dataset[:,-1]

# Xu ly du lieu date
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OrdinalEncoder    # ma hoa thanh cac so nguyen lien tiep
ct = ColumnTransformer(transformers=[('encoder', OrdinalEncoder(), [0])], remainder='passthrough')
X = np.array(ct.fit_transform(X))

# xu ly du lieu bi thieu.
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
imputer.fit(X)                          # xu ly nhung cot kieu so  
X = imputer.transform(X)

# chia tap du lieu ban dau thanh 2 tap: Train - Test
from sklearn.model_selection import train_test_split
# Luu y: test_size = 0.2 co nghia la lay 20% de Test - 80% Train
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state= 1)

# Khai bao mo hinh mong muon (RandomForestRegressor)
from sklearn.ensemble import RandomForestRegressor
# Cai dat mo hinh voi cac tham so thuat toan
regressor = RandomForestRegressor(n_estimators = 10, random_state = 0)
# huan luyen dua tren tap du lieu (Train)
regressor.fit(X_train, y_train)

# Danh gia
# voi bai toan Hoi quy (regression): R2 score
print(regressor.score(X_test, y_test))

#Tim kiem bo tham so toi uu
from sklearn.model_selection import GridSearchCV
parameters = [{
                'n_estimators' : [20,50,100],
                'criterion' : ["squared_error", "absolute_error", "poisson"]
                }]
# chay thuat toan tim kiem tham so toi uu trong mien xac dinh
# neu co nhieu du lieu, cv = 5 - 10
grid_search = GridSearchCV(estimator = regressor,
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 2,
                           n_jobs = -1)
grid_search.fit(X_train, y_train)
best_accuracy = grid_search.best_score_
best_parameters = grid_search.best_params_
print("Best Accuracy: {:.2f} %".format(best_accuracy*100))
print("Best Parameters:", best_parameters)


# save the model to disk
filename = 'model_reg.sav'
pickle.dump(regressor, open(filename, 'wb'))
# # Visualising the Random Forest Regression results (higher resolution)
# X_grid = np.arange(min(X), max(X), 0.01)
# X_grid = X_grid.reshape((len(X_grid), 1))
# plt.scatter(X, y, color = 'red')
# plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
# plt.title('Truth or Bluff (Random Forest Regression)')
# plt.xlabel('Position level')
# plt.ylabel('Salary')
# plt.show()