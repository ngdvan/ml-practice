import pickle
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
import numpy as np

filename = 'model_reg.sav'
loaded_model = pickle.load(open(filename, 'rb'))
# from sklearn.preprocessing import StandardScaler
# sc = pickle.load(open('std_scaler.bin', 'rb'))

dataset = pd.read_excel('kt.xlsx')
X_test = dataset.iloc[:, :-1].values                #doc toan bo Hang, cot doc tu 0 -> truoc cot cuoi
y_test = dataset.iloc[:, -1].values  
y_test *= 1e5


result = loaded_model.predict(X_test) / 1e5

np.savetxt('ret.csv', result)