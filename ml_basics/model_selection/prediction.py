import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor


dataset = np.loadtxt('../data.reg', delimiter=' ')
X = dataset[:,:-1]
y = dataset[:,-1]
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state= 1)

estimator = MLPRegressor(activation="tanh", solver = "lbfgs", hidden_layer_sizes=(4,))
# Training
estimator.fit(X_train, y_train)
# R2 Score
r2_score = estimator.score(X_test, y_test)
# prediction result
y_predict = estimator.predict(X_test)
margin = y_predict - y_test

averageErr = np.average(margin)
stdRaw = np.std(margin)

print("R2 score: ", r2_score)
print("Average error:", averageErr)
print("Standard Deviation:", stdRaw)
