import sys
import numpy as np
from sklearn import datasets
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

datasetMode = 1    #datasetMode: 1 classification; 2 regression
# Command Line agrs
if len(sys.argv) >= 2:
    datasetMode = int(sys.argv[1])

# Classification
if datasetMode == 1:
    X, y = datasets.make_classification(n_samples=500, 
    n_features=5, n_classes=2, n_redundant=0,
    n_clusters_per_class=1, weights=[0.7, 0.3], random_state=1)
    
    fig, ax = plt.subplots(figsize=(6, 6))
    plt.xlabel("X0", fontsize=16)
    plt.ylabel("X1", fontsize=16)
    plt.scatter(X[:,0], X[:,1], s=60, c=y)
    plt.show()
    dataset = np.column_stack((X, y))
    np.savetxt("../data.class",dataset)

# Regression
else:
    X, y = datasets.make_regression(n_samples=500, n_features=5, n_informative=3, random_state=50)
    
    df = pd.DataFrame(X)
    
    df.columns = ['ftre1', 'ftre2', 'ftre3', 'ftre4', 'ftre5']
    df['target'] = y
    # Determine correlations
    corr = df.corr()
    # Draw the correlation heatmap
    f, ax = plt.subplots(figsize=(9, 6))
    mask = np.triu(np.ones_like(corr, dtype=bool))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(corr, annot=True, mask = mask, cmap=cmap)
    plt.show()
    dataset = np.column_stack((X, y))
    np.savetxt("../data.reg",dataset)    
    