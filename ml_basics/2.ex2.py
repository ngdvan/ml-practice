from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
predictor = MLPRegressor(random_state=2,hidden_layer_sizes = (5,2), activation="tanh", solver="lbfgs", max_iter=1000)

# Importing the dataset
dataset = pd.read_csv('sample.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)
predictor.fit(X_train, y_train)
print(X_test)
print(predictor.predict(X_test))
print(predictor.score(X_test,y_test))