# # int float type
# x = 3
# print(type(x))  # class<int>
# print(x + 1)    # 4
# print(x - 2)    # 1
# print(x / 3)    # 1.0
# x += 1          # x = x + 1
# print(x)        # 4
# x *= 2          
# print(x)        # 8

# boolean type
# t = True
# f = False
# print( t and f)
# print( t or f)
# print( not f)
# print( t != f)
# print ( t == f)

str = "hello"
print(str)
print(len(str))
print(str + " world")
print(str[0])
print(str.upper())
print(str.find('m'))