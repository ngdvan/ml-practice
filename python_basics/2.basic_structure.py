# List structure
lst = [3, 4, 6, 7, "hello"]
# print(lst[0])
# print(lst[-1])
# print(lst.pop(1))
# lst.append(10)
# lst.append(9)
# print(lst)
# print(lst[1:5])
# print(lst[1:5:2])
# lst.sort()
# print(lst)
# print(lst[4])

# Dictionary
# d = {'car': 'Toyota', 'A': 100, 'B':150}
# print(d['car'])
# print('car' in d)
# d['bike'] = 'MTB'
# print(d.get('ship'))
# print(d.keys(), d.values())

# Set
# pet = {'cat', 'dog'}
# print('cat' in pet)
# pet.add('fish')
# pet.add('cat')
# print(pet)
# pet.remove('dog')

# Tuple
t = (5, 6, 7)
a, b,c = t
print(a, b, c)
print(t[0])
t2 = t + (1, 2)
print(t2[1])