import numpy as np
# lst = [1,2,4,5]
# a = np.array(lst)
# print(a)
# a[0] = 5
# print(a[0], a[1])

# b = np.array([[1,2,3], [4,5,6]])
# print(b.shape)  # (2,3)
# print(b[0,1])

# a = np.zeros((2,2))
# print(a)
# b = np.ones((1,2))
# print(b)
# c = np.full((2,2), 5)
# print(c)
# d = np.eye(2)
# print(d)
# e = np.random.random((2,2))
# print(e)
# f = np.arange(0, 10, 1, dtype=int)
# print(f)

# a = np.array([[1,2,3], [4,5,6], [7,8,9]])
# print(a.shape)
# b = a[1:3, 1:3]
# print(b)
# print(a[0,1])
# c = np.transpose(a)
# print(c)

# print(np.mean(a, axis=0))
a = np.array([[1,2,3], [4,5,6], [7,8,9]])
boolIdx = (a > 5)
print(boolIdx)
print(a[boolIdx])
print(a[a>5])