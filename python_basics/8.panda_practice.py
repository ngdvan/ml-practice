import pandas as pd


# print(df)
# print(df.info())
# print(df.describe())
# print(df['Age'].value_counts())
# set1 = df[ df['pos'] == 'quanly']
# print(set1)
# set1 = set1.drop(['Name'], axis="columns")
# set1 = set1.drop([0,1], axis='rows')
# print(set1)


# Thêm thư viện vẽ đồ thị
import matplotlib.pyplot as plt
# Load dữ liệu
df = pd.read_excel('data.xlsx', 'Sheet1')
df['time'] = pd.to_datetime(df['time'], errors="coerce")
df.dropna(subset=["time"], inplace=True)
print(df)
# # Loại bỏ cột dữ liệu Salary (Ví dụ)
# df1 = df.drop("Salary", axis="columns")
# # Vẽ đồ thị có kiểu historgram
# df1.plot(kind="density",  xlabel = "person", grid=True)
# # Hiển thị đồ thị
# plt.show()

# import seaborn as sns
# import numpy as np
# corr = df.corr()
# f, ax = plt.subplots()
# mask = np.triu(np.ones_like(corr, dtype=bool))
# cmap = sns.diverging_palette(230, 20, as_cmap=True)
# sns.heatmap(corr, annot=True, mask = mask, cmap=cmap)
# plt.show()

for x in df.index:
    if df.loc[x, "Salary"] > 3000:
        df.loc[x, "Salary"] = df['Salary'].mean()
print(df.duplicated())
df.drop_duplicates(inplace=True)
print(df)

df.to_excel('out.xlsx')
