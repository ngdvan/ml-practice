class Robot():
    # Constructor
    def __init__(self, name):
        self.name = name

    def introduce(self, short=False):
        if not short:
            print("Hello, my name is %s" % self.name)
        else:
            print("I am %s" % self.name)

robot = Robot("May")
robot2 = Robot("melis")
robot.introduce()
robot.introduce(short=True)