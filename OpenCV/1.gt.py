import cv2
import numpy as np
from matplotlib import pyplot as plt

# Load image
image = cv2.imread("images/button.jpg")
Ismall = cv2.resize(image, None, fx=0.5, fy=0.5)

gray = cv2.cvtColor(Ismall, cv2.COLOR_BGR2GRAY)

gray = cv2.GaussianBlur(gray, (7, 7), 0)
cv2.imshow("Blur", gray)

ret3,thresh1 = cv2.threshold(gray,130,255,cv2.THRESH_BINARY)
cv2.imshow("thresh1", thresh1)
# thresh1 = cv2.adaptiveThreshold(gray, 180, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 3, 5) 
# cv2.imshow("Adaptive Mean Thresholding", thresh1) 

kernel = np.ones((5,5), np.uint8)
erode = cv2.erode(thresh1, kernel, iterations=1   )
erodeSmall = cv2.resize(erode, None, fx=0.5, fy=0.5)
cv2.imshow('Erodsion',erodeSmall)

thresh1 = 255-erode

# Set our filtering parameters
# Initialize parameter settiing using cv2.SimpleBlobDetector
params = cv2.SimpleBlobDetector_Params()

# Set Area filtering parameters
params.filterByArea = True
params.minArea = 120

# Set Circularity filtering parameters
params.filterByCircularity = True 
params.minCircularity = 0.8

# Set Convexity filtering parameters
# params.filterByConvexity 9= False
# params.minConvexity = 0.01

# Create a detector with the parameters
detector = cv2.SimpleBlobDetector_create(params)
    
# Detect blobs
keypoints = detector.detect(thresh1)
# print(keypoints.shape)``

reddot = 0
redKeyPoint = []
greenKeyPoint = []
yellowKeyPoint = []
delta = 7
for i,k in enumerate(keypoints):
    x, y = k.pt
    avg = Ismall[int(y) - delta:int(y) + delta,int(x)-delta:int(x)+delta]
    avg_r = np.mean(avg[:,:,2])
    avg_g = np.mean(avg[:,:,1])
    avg_b = np.mean(avg[:,:,0])
    # print(x,y)
    if avg_r > 200 and avg_g < 200:
        redKeyPoint.append(k)
    elif avg_g > 200 and avg_r < 200 :
        greenKeyPoint.append(k)
    else:
        print(avg_b, avg_g, avg_r)
        yellowKeyPoint.append(k)

# Draw blobs on our image as red circles
blank = np.zeros((1,1)) 
# Red
blobs = cv2.drawKeypoints(Ismall, redKeyPoint, blank, (255,0,0),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Green
blobs = cv2.drawKeypoints(blobs, greenKeyPoint, blank, (0,0,255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

blobs = cv2.drawKeypoints(blobs, yellowKeyPoint, blank, (255,255,0),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)



# number_of_blobs = reddot
text = "Number of Circular Blobs: "+ " Red: "  + str(len(redKeyPoint)) + " Green: " + str(len(greenKeyPoint))+ " Yellow: " + str(len(yellowKeyPoint))
cv2.putText(blobs, text, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 100, 255), 2)
if len(redKeyPoint) > 0:
    text = "Co loi!!!! "+ " Red: "  + str(len(redKeyPoint))
    cv2.putText(blobs, text, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 100, 255), 2)
blobs = cv2.resize(blobs, None, fx=0.75, fy=0.5)
cv2.imshow("Du Bao",blobs)
cv2.waitKey(0)

